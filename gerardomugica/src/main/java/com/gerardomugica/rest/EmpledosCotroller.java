package com.gerardomugica.rest;


import com.gerardomugica.rest.empleados.Capacitacion;
import com.gerardomugica.rest.empleados.Empleado;
import com.gerardomugica.rest.empleados.Empleados;
import com.gerardomugica.rest.repositorios.EmpleadoDAO;
import com.gerardomugica.rest.utils.Configuracion;
import com.gerardomugica.rest.utils.Utilidades;
import org.apache.logging.log4j.spi.ObjectThreadContextMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path= "/empleados")

public class EmpledosCotroller {
    @Autowired
    private EmpleadoDAO empDao;

    @GetMapping(path = "/")
    public Empleados getEmpleados() {
        return empDao.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    //public Empleado getEmpleado(@PathVariable int id) {
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id) {
        //empDao.getEmpleado(id);
        Empleado emp = empDao.getEmpleado(id);
        if (emp == null) {
            return ResponseEntity.notFound().build();   //para que regrese el 404 not found
        } else {
            return ResponseEntity.ok().body(emp);       //Para que regrese los datos emp
        }
        //return empDao.getEmpleado(id);
    }

    @PostMapping("/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        Integer id = empDao.getAllEmpleados().getListaEmpledos().size() + 1;
        emp.setId(id);
        empDao.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp) {
        empDao.updEmpleado(emp);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id, @RequestBody Empleado emp) {
        empDao.updEmpleado(id, emp);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleadoXId(@PathVariable int id) {
        String resp = empDao.deleteEmpleado(id);
        if (resp == "OK") {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> softupdEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates) {
        empDao.softupdEmpleado(id, updates);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpledo(@PathVariable int id) {
        return ResponseEntity.ok().body(empDao.getCapacitacionesEmpleado(id));
    }

    @PostMapping(path = "/{id}/capacitaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addCapacitacionEmpleado(@PathVariable int id, @RequestBody Capacitacion cap) {
        if (empDao.addCapacitacion(id, cap)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @Value("${app.titulo}")
    private String titulo;

    @GetMapping("/titulo")
    public String getAppTitulo() {
        String modo = configuracion.getModo();
        return String.format("(%s) (%s)", titulo, modo);
        //return titulo;
    }

    @Autowired
    private Environment env;

    @GetMapping("/autor")
    public String getAppAutor() {
        //return env.getProperty("app.autor");
        return configuracion.getAutor();
    }

    @Autowired
    Configuracion configuracion;

    @GetMapping("/cadena")
    public String getAutor() {
        return configuracion.getAutor();
    }

    @GetMapping("/cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String separador) {
        try {
            return Utilidades.getCadena(texto, separador);
        } catch (Exception e) {
            return "";
        }
    }
}

