package com.gerardomugica.rest.empleados;

import java.util.ArrayList;
import java.util.List;

public class Empleados {
    private List<Empleado> listaEmpleados;

    public List<Empleado> getListaEmpledos() {
        if (listaEmpleados == null) {
            listaEmpleados = new ArrayList<>();
        }
        return listaEmpleados;
    }
    public void setListaEmpleados(List<Empleado> listaEmpleados){
        this.listaEmpleados = listaEmpleados;
    }
}
