package com.gerardomugica.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collection;
import java.util.Collections;

import static org.springframework.boot.web.servlet.server.Session.SessionTrackingMode.URL;
import static org.springframework.hateoas.IanaLinkRelations.LICENSE;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.gerardomugica.rest" + ""))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "The rest emp api", "Devuelve informacion de empleados", "1.0",
                "http://codm", new Contact("Demo", "https://dem", "apis@demo.com"),
                        "LICENSE URL", "https://dem", Collections.emptyList());
    }
}
