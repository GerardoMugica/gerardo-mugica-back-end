package com.gerardomugica.rest.utils;

public enum EstadoPedido {
        ACEPTADO,
        COCINADO,
        EN_ENTREGA,
        ENTREGADO,
        VALORADO;
}
