package com.gerardomugica.rest;

import com.gerardomugica.rest.utils.BadSeparator;
import com.gerardomugica.rest.utils.EstadoPedido;
import com.gerardomugica.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpledosCotroller empledosCotroller;
    @Test
    public void testGetCadena() {
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "Luz del sol";
        assertEquals(correcto, empledosCotroller.getCadena(origen, "."));
    }

    @Test
    public void testSeparadorGetCadean() {
        try {
            Utilidades.getCadena("Gerardo Mugica", ".");
            fail("Se esperaba BadSeparator");
        } catch (BadSeparator bs) {
        }
    }

    @Test
    public void testGetAutor() {
        assertEquals("Gerardo Mugica", empledosCotroller.getAutor());
    }

    //Testei Parametrizadof
    @ParameterizedTest
    //Donde se van a definir los valores.
    @ValueSource (ints = {1,3,5, 16, -3, Integer.MAX_VALUE})
    public void testEsImpar(int numero) {
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource (strings = {" ", "", })
    public void testEstaBlanco(String texto) {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource (strings = {"", "/t", "/n"})
    public void testEstaBlancoCompleto(String texto) {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadoPedido.class)
    public void testValorarEstadoPedido(EstadoPedido ep) {
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }


}
