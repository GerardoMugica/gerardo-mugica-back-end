package com.gerardomugica.servicios;

import org.json.JSONML;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")

public class ServiciosController {
    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filto){
        return ServiciosService.getAll();
    }
    @PostMapping("/servicios")
    public String setServicio(@RequestBody String newServicio){
        try {
            ServiciosService.insert(newServicio);
            return "OK";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }

    @PutMapping("/servicios")
    public String updServicios(@RequestBody String data) {
        try {
            JSONObject obj = new JSONObject(data);
            String filto = obj.getJSONObject("filtro").toString();
            String update = obj.getJSONObject("update").toString();
            ServiciosService.update(filto, update);
            return "OK";
        } catch (Exception ex) {
            return  ex.getMessage();
        }
    }

}
