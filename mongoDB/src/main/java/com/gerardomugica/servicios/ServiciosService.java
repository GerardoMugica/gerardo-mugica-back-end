package com.gerardomugica.servicios;
import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.MongoClient;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ServiciosService {
    static MongoCollection<Document> servicios;
    private static MongoCollection<Document> getServiciosCollection()
    {
        ConnectionString cs =new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongo= MongoClients.create(settings);
        MongoDatabase database= mongo.getDatabase("dbprod");
        return database.getCollection("servicios");

    }

    public static void insert(String servicio) throws  Exception
    {
        Document doc = Document.parse(servicio);
        servicios= getServiciosCollection();
        servicios.insertOne(doc);
    }

    public static void insertBatch(String strServicios) throws Exception {
        servicios = getServiciosCollection();
        Document doc = Document.parse(strServicios);
        List<Document> lst = doc.getList("servicios", Document.class);
        if (lst == null) {
            servicios.insertOne(doc);
        } else {
            servicios.insertMany(lst);
        }
    }

    public static List getAll()
    {
        servicios= getServiciosCollection();
        List lst=new ArrayList();
        FindIterable<Document> iterdoc = servicios.find();
        Iterator it = iterdoc.iterator();
        while (it.hasNext())
        {
            lst.add(it.next()) ;
        }
        return lst;
    }
    public static List getFiltados(String filtro) {
        servicios = getServiciosCollection();
        List lst = new ArrayList();
        Document docFiltro = Document.parse(filtro);
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lst.add(it.next());
        }
        return lst;
    }

    public static void update(String filtro, String servicio)
    {
        servicios= getServiciosCollection();
        Document docfiltro = Document.parse(filtro);
        Document doc= Document.parse(servicio);
        servicios.updateOne(docfiltro,doc);
    }
}